$(function () {
	$('.play-video').magnificPopup({
		disableOn: 0,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
})

/* ======================= Делаем мультиязычный сайт c JavaScript на Ютубе ============== */

const allLang = ['en', 'ru', 'tat'];
const langs = document.querySelectorAll('.language__link');

langs.forEach(function (elem) {
	elem.addEventListener('click', function (e) {
		e.preventDefault();
		changeURLLanguage(elem);
		changeLanguage();
	})

});

function changeURLLanguage(elem) {
	const lang = elem.getAttribute('data-lng');
	location.href = window.location.pathname + '#' + lang;
	/* location.reload(); */
}

function changeLanguage() {
	let hash = window.location.hash;
	hash = hash.substring(1);
	if (!allLang.includes(hash)) {
		location.href = window.location.pathname + '#ru';
		location.reload();
	}
	document.querySelector('title').innerHTML = langArr['title'][hash];
	for (let key in langArr) {
		let elem = document.querySelector('.lng-' + key);
		if (elem) {
			elem.innerHTML = langArr[key][hash];
		}
	}
}
/* changeLanguage(); */

/* document.addEventListener("DOMContentLoaded", function () { // используем событие загрузки страницы, не включая картинки и прочее
	let iframes = document.querySelectorAll('.iframeAdaptive');
	iframes.forEach(function (i) { // перебираем имеющиеся Iframe с присвоенным нами классом
		let iframeWidth = i.width; // берём из атрибута width ширину
		let iframeHeight = i.height; // берём из атрибута height высоту
		console.log(i);
		let iframeParent = i.parentNode; // определяем родительский элемент нашего Iframe
		let parentWidth = parseInt(getComputedStyle(iframeParent)['width']) - parseInt(getComputedStyle(iframeParent)['padding-left']) - parseInt(getComputedStyle(iframeParent)['padding-right']) - 10; // берём родительский контейнер и высчитываем нужную нам ширину, без учёта padding, margin и border
		let iframeProportion = parentWidth / iframeWidth;
		i.setAttribute('width', parentWidth); // устанавливаем ширину нашим Iframe
		i.setAttribute('height', iframeHeight * iframeProportion); // устанавливаем высоту нашим Iframe
	});
}); */