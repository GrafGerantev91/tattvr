const langArr = {
	"title": {
		"ru": "ТВР",
		"en": "TVR",
		"tat": "ТВР"
	},
	"main__subtitle": {
		"ru": "ТВР - это самые популярные телеканалы и радиостанции Республики Татарстан на Smart TV и в мобильном приложении Android и IOS",
		"en": "TVR is the most popular TV channels and radio stations of the Republic of Tatarstan on Smart TV and in the Android and IOS mobile application",
		"tat": "TVR - Смарт ТВда һәм Android һәм IOS мобиль кушымталарында Татарстан Республикасының иң популяр телеканаллары һәм радиостанцияләре."
	},
	"main__title": {
		"ru": "ТВР - смотрит вся Республика, смотрит весь мир!",
		"en": "TVR - the whole Republic is watching, the whole world is watching!",
		"tat": "TVR - бөтен Республика карый, бөтен дөнья карый!"
	},
	"setuptv__title": {
		"ru": "Инструкция по настройке Smart TV",
		"en": "Instructions for setting up Smart TV",
		"tat": "Смарт ТВ урнаштыру өчен инструкция"
	},
	"setuptv__video-title": {
		"ru": "Видеоинструкция по настройке Smart TV",
		"en": "Video instruction for setting up Smart TV",
		"tat": "Смарт ТВ урнаштыру өчен видео күрсәтмә"
	},
	"setupmobile__title": {
		"ru": "Скачать приложение",
		"en": "Download app",
		"tat": "Кушымтаны йөкләү"
	},
	"setupmobile__link": {
		"ru": "Инструкция по использованию приложения",
		"en": "Instructions for using the application",
		"tat": "Кушымтаны куллану өчен инструкция"
	},
	"channelstv__title": {
		"ru": "Список телеканалов:",
		"en": "List of TV channels:",
		"tat": "Телеканаллар исемлеге:"
	},
	"newcentury__title": {
		"ru": "Татарстан Новый век",
		"en": "Tatarstan new century",
		"tat": "Татарстан яңа гасыр"
	},

	"newcentury__text": {
		"ru": "телеканал о жизни республики, который вещает на двух языках - татарском и русском. Новости, общественно-политические программы, телесериалы, ток-шоу, прямые трансляции спортивных матчей, зрелищные мероприятия на родном для Татарстана языке.",
		"en": "TV channel about the life of the republic, which broadcasts in two languages - Tatar and Russian. News, socio-political programs, TV series, talk shows, live broadcasts of sports matches, entertainment events in the native language of Tatarstan.",
		"tat": "ике телдә - татар һәм рус телләрендә трансляцияләүче республика тормышы турында телеканал. Яңалыклар, иҗтимагый-сәяси программалар, сериаллар, ток-шоулар, спорт матчларының туры эфиры, Татарстанның туган телендәге күңел ачу чаралары."
	},

	"efir": {
		"ru": 'телеканал "Эфир"',
		"en": "TV channel Efir",
		"tat": '"Эфир" телеканалы'
	},
	"tat24": {
		"ru": "Татарстан-24",
		"en": "Tatarstan-24",
		"tat": "Татарстан-24"
	},
	"univer": {
		"ru": "Универ TV",
		"en": "Univer TV",
		"tat": "Универ TV"
	},
	"channelsradio__title": {
		"ru": "Список радиостанций:",
		"en": "List of radio stations:",
		"tat": "Радиостанцияләр исемлеге:"
	},
	"chanson": {
		"ru": "Шансон",
		"en": "Chanson",
		"tat": "Шансон"
	},
	"echo": {
		"ru": "Эхо",
		"en": "Echo",
		"tat": "Эхо"
	},
	"iskatel": {
		"ru": "Искатель",
		"en": "Seeker",
		"tat": "Эзләүче"
	},
	"footer__text": {
		"ru": 'Контакты оператора связи и службы поддержки: ПАО "Радиотелеком"',
		"en": 'Contacts of the telecom operator and support service: PJSC "Radiotelecom"',
		"tat": 'Телеком операторы һәм ярдәм хезмәтенең контактлары: "Радиотелеком" PJSC'
	},
	"footer__phone": {
		"ru": "Телефон:",
		"en": "Phone:",
		"tat": "Телефон:"
	},
	"footer__email": {
		"ru": "Электронная почта:",
		"en": "Email:",
		"tat": "Электрон почта:"
	},

}